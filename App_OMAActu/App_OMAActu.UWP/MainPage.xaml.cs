﻿namespace App_OMAActu.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            LoadApplication(new App_OMAActu.App());
        }
    }
}