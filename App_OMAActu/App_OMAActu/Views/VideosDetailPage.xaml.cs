﻿using App_OMAActu.ViewModels;
using Xamarin.Forms;

namespace App_OMAActu.Views
{
    public partial class VideosDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;
        public VideosDetailPage()
        {
            InitializeComponent();

        }
        public VideosDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;

        }
    }
}