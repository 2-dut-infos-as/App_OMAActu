﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_OMAActu.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuNavigationPage : ContentPage
    {
       
        public MenuNavigationPage()
        {
            InitializeComponent();

            
            var menuNavigationPageItems = new List<MenuNavigationItem>();

            menuNavigationPageItems.Add(new MenuNavigationItem
            {
                Title = "Actulités",
                IconSource = "actualites.png",
                TargetType = typeof(ActualitesPage)
            });

            menuNavigationPageItems.Add(new MenuNavigationItem
            {
                Title = "Info en continue",
                IconSource = "Information.png",
                TargetType = typeof(DepechesPage)
            });
            

            listView.ItemsSource = menuNavigationPageItems;

            BindingContext  = listView.ItemsSource;

            
        }
    }
}
