﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using App_OMAActu.Models;
using App_OMAActu.ViewModels;




namespace App_OMAActu.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DepechesPage : ContentPage
    {
        DepechesViewModel viewModel;
        //----------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        public DepechesPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new DepechesViewModel();
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MenuNavigationPage());
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------
    }
}
