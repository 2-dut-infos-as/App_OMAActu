﻿using App_OMAActu.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_OMAActu.Views
{
    
    public partial class MesFavorisDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;
        public MesFavorisDetailPage()
        {
            InitializeComponent();
        }

        public MesFavorisDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;
        }
    }
}