﻿using System;

using App_OMAActu.Models;
using App_OMAActu.ViewModels;

using Xamarin.Forms;

namespace App_OMAActu.Views
{

    public partial class VideosPage : ContentPage
    {
        VideosViewModel viewModel;
        //----------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        public VideosPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new VideosViewModel();
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as VideosItem;
            if (item == null)
                return;

            await Navigation.PushAsync(new VideosDetailPage(new ItemDetailViewModel(item)));

            // Manually deselect item
            ItemsListView.SelectedItem = null;
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------
        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MenuNavigationPage());
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------
    }
}
