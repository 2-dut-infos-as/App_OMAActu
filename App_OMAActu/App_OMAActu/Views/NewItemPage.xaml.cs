﻿using System;

using App_OMAActu.Models;

using Xamarin.Forms;

namespace App_OMAActu.Views
{
    public partial class NewItemPage : ContentPage
    {
        public ActualitesItem Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();

            Item = new ActualitesItem
            {
                titre = "Item name",
                description = "This is a nice description"
            };

            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "AddItem", Item);
            await Navigation.PopToRootAsync();
        }
    }
}