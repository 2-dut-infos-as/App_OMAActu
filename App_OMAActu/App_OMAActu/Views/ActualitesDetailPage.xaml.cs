﻿
using App_OMAActu.ViewModels;

using Xamarin.Forms;

namespace App_OMAActu.Views
{
    public partial class ActualitesDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;
        

        // Note - The Xamarin.Forms Previewer requires a default, parameterless constructor to render a page.
        public ActualitesDetailPage()
        {
            InitializeComponent();


        }

        public ActualitesDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;

           }
        
    }
}
