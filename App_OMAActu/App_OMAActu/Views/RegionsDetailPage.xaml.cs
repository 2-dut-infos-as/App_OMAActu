﻿
using Xamarin.Forms;

using App_OMAActu.ViewModels;


namespace App_OMAActu.Views
{
    public partial class RegionsDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;
        public RegionsDetailPage()
        {
            InitializeComponent();

        }
        public RegionsDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;

        }
    }
}