﻿using System;

using App_OMAActu.Models;
using App_OMAActu.ViewModels;

using Xamarin.Forms;
using Windows.UI.Xaml;

namespace App_OMAActu.Views
{
    public partial class ActualitesPage : ContentPage
    {
        ActualitesViewModel viewModel;
        //----------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        public ActualitesPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ActualitesViewModel();
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as ActualitesItem;
            if (item == null)
                return;

            await Navigation.PushAsync(new ActualitesDetailPage(new ItemDetailViewModel(item)));

            // Manually deselect item
            ItemsListView.SelectedItem = null;
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------
        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MenuNavigationPage());
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
        //----------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------

        
    }
}
