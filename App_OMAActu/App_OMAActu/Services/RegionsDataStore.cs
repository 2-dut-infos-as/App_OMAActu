﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App_OMAActu.Models;
using System.Net.Http.Headers;
using System.Net.Http;
using Xamarin.Forms;
using System.Web;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Net;
using WebApiContrib.Formatting;
using System.Diagnostics;


[assembly: Dependency(typeof(App_OMAActu.Services.RegionsDataStore))]
namespace App_OMAActu.Services
{
    public class RegionsDataStore : InterfaceRegions<RegionsItem>
    {

        bool isInitialized;
        List<RegionsItem> items;
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> AddItemAsync(RegionsItem item)
        {
            await InitializeAsync();

            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> UpdateItemAsync(RegionsItem item)
        {
            await InitializeAsync();

            var _item = items.Where((RegionsItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);
            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> DeleteItemAsync(RegionsItem item)
        {
            await InitializeAsync();

            var _item = items.Where((RegionsItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);

            return await Task.FromResult(true);
        }

        public async Task<RegionsItem> GetItemAsync(string id)
        {
            await InitializeAsync();

            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }
        //--------------------------------------------------------------------------------------------------------------------------------
        public async Task<IEnumerable<RegionsItem>> GetItemsAsync(bool forceRefresh = false)
        {
            await InitializeAsync();

            return await Task.FromResult(items);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> PullLatestAsync()
        {
            return Task.FromResult(true);
        }
        //-------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> SyncAsync()
        {
            return Task.FromResult(true);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task InitializeAsync()
        {
            if (isInitialized)
                return;

            items = new List<RegionsItem>();
            //-----------------------------------------------------------------------------------------------------------------------------
            //REGIONS
            //-----------------------------------------------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------------------------------------------
            // DYNAMIQUE
            //---------------------------------------------------------------------------------------------------------------------------

            HttpClient client = new HttpClient();

            // Download string.
            string value = await client.GetStringAsync("https://api.myjson.com/bins/ugas7");


            var _items = JsonConvert.DeserializeObject<List<RegionsItem>>(value);


            foreach (var obj in _items)
            {
                RegionsItem regions = obj;

                regions.Id = Guid.NewGuid().ToString();
                regions.WebSource = RegionsItem.WebViewSource(obj.contenue);

                //actualites.images = obj.images;
            }
            


            foreach (RegionsItem item in _items)
            {
                items.Add(item);
            }


            //---------------------------------------------------------------------------------------------------------------------------
            // FIN DYNAMIQUE
            //---------------------------------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------------------------------
            //FIN REGIONS
            //-----------------------------------------------------------------------------------------------------------------------------
            isInitialized = true;
        }
    }
}
