﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App_OMAActu.Models;
using System.Net.Http.Headers;
using System.Net.Http;
using Xamarin.Forms;
using System.Web;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Net;
using WebApiContrib.Formatting;
using System.Diagnostics;
using Java.IO;

[assembly: Dependency(typeof(App_OMAActu.Services.VideosDataStore))]
namespace App_OMAActu.Services
{
    public class VideosDataStore : InterfaceVideos<VideosItem>
    {

        bool isInitialized;
        List<VideosItem> items;
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> AddItemAsync(VideosItem item)
        {
            await InitializeAsync();

            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> UpdateItemAsync(VideosItem item)
        {
            await InitializeAsync();

            var _item = items.Where((VideosItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);
            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> DeleteItemAsync(VideosItem item)
        {
            await InitializeAsync();

            var _item = items.Where((VideosItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);

            return await Task.FromResult(true);
        }

        public async Task<VideosItem> GetItemAsync(string id)
        {
            await InitializeAsync();

            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }
        //--------------------------------------------------------------------------------------------------------------------------------
        public async Task<IEnumerable<VideosItem>> GetItemsAsync(bool forceRefresh = false)
        {
            await InitializeAsync();

            return await Task.FromResult(items);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> PullLatestAsync()
        {
            return Task.FromResult(true);
        }
        //-------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> SyncAsync()
        {
            return Task.FromResult(true);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task InitializeAsync()
        {
            if (isInitialized)
                return;

            items = new List<VideosItem>();
            //-----------------------------------------------------------------------------------------------------------------------------
            //Video
            //-----------------------------------------------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------------------------------------------
            // DYNAMIQUE
            //---------------------------------------------------------------------------------------------------------------------------

            HttpClient client = new HttpClient();

            // Download string.
            string value = await client.GetStringAsync("https://api.myjson.com/bins/15rj9z");


            var _items = JsonConvert.DeserializeObject<List<VideosItem>>(value);


            foreach (var obj in _items)
            {
                VideosItem videos = obj;

                videos.Id = Guid.NewGuid().ToString();
                /*string[] ret = obj.contenue.Split('/');
                Debug.WriteLine(ret);
                String result = "";
                for (int i = 1; i < ret.Length; i++)
                {
                    result = String.Concat(result, ret[i]);
                }
                string lien_video = String.Concat(ret[0] + "https:" + result);
                */
                String liens = obj.contenue;
                String result = liens.Replace("src=\"", "src=\"https:");
                Debug.WriteLine(result);
                videos.WebSource = VideosItem.WebViewSource(result);

                
            }


            foreach (VideosItem item in _items)
            {
                items.Add(item);
            }


            //---------------------------------------------------------------------------------------------------------------------------
            // FIN DYNAMIQUE
            //---------------------------------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------------------------------
            //FIN video
            //-----------------------------------------------------------------------------------------------------------------------------
            isInitialized = true;
        }
    }
}
