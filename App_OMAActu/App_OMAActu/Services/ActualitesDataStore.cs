﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App_OMAActu.Models;
using System.Net.Http.Headers;
using System.Net.Http;
using Xamarin.Forms;
using System.Web;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Net;
using WebApiContrib.Formatting;
using System.Diagnostics;
using Java.IO;
using static Android.Resource;

[assembly: Dependency(typeof(App_OMAActu.Services.ActualitesDataStore))]
namespace App_OMAActu.Services
{
    public class ActualitesDataStore : InterfaceActualites<ActualitesItem>
    {
        bool isInitialized;
        List<ActualitesItem> items;
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> AddItemAsync(ActualitesItem item)
        {
            await InitializeAsync();

            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> UpdateItemAsync(ActualitesItem item)
        {
            await InitializeAsync();

            var _item = items.Where((ActualitesItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);
            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> DeleteItemAsync(ActualitesItem item)
        {
            await InitializeAsync();

            var _item = items.Where((ActualitesItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);

            return await Task.FromResult(true);
        }

        public async Task<ActualitesItem> GetItemAsync(string id)
        {
            await InitializeAsync();

            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }
        //--------------------------------------------------------------------------------------------------------------------------------
        public async Task<IEnumerable<ActualitesItem>> GetItemsAsync(bool forceRefresh = false)
        {
            await InitializeAsync();

            return await Task.FromResult(items);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> PullLatestAsync()
        {
            return Task.FromResult(true);
        }
        //-------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> SyncAsync()
        {
            return Task.FromResult(true);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task InitializeAsync()
        {
            if (isInitialized)
                return;

            items = new List<ActualitesItem>();
            //-----------------------------------------------------------------------------------------------------------------------------
            //ACTUALITES
            //-----------------------------------------------------------------------------------------------------------------------------

            //---------------------------------------------------------------------------------------------------------------------------
            // DYNAMIQUE
            //---------------------------------------------------------------------------------------------------------------------------
           
            HttpClient client = new HttpClient();

            // Download string.
            string value = await client.GetStringAsync("https://api.myjson.com/bins/qz98n");

        
            var _items = JsonConvert.DeserializeObject< List<ActualitesItem>> (value);


            foreach (var obj in _items)
            {
                ActualitesItem actualites = obj;

                actualites.Id = Guid.NewGuid().ToString();
                actualites.WebSource = ActualitesItem.WebViewSource(obj.contenue);

                //actualites.images = obj.images;
            }
            
           
            foreach (ActualitesItem item in _items)
            {
                items.Add(item);
            }

           
            //---------------------------------------------------------------------------------------------------------------------------
            // FIN DYNAMIQUE
            //---------------------------------------------------------------------------------------------------------------------------

            //---------------------------------------------------------------------------------------------------------------------------
            // STATIC
            //---------------------------------------------------------------------------------------------------------------------------
            /*    var _items = new List<ActualitesItem>();

                        _items = new List<ActualitesItem>
                        {
                        new ActualitesItem { Id = Guid.NewGuid().ToString(),
                            Images ="cm_110117.jpg",
                            Titre ="Le conseil du gouvernement d’hier",
                            Description ="Le conseil du gouvernement d’hier (02/07/2013) a approuvé la communication relative d’un protocole d’entente entre la République malgache et le programme EDES-COLEAP concernant l’amélioration du système de sécurité sanitaire des aliments à Madagascar. Le lancement officiel de ce programme se tiendra dans le courant de la première semaine de ce mois de juillet. Ce programme se focalisera d’abord sur la filière litchi.",
                            Contenue ="Le conseil du gouvernement d’hier (02/07/2013) a approuvé la communication relative d’un protocole d’entente entre la République malgache et le programme EDES-COLEAP concernant l’amélioration du système de sécurité sanitaire des aliments à Madagascar. Le lancement officiel de ce programme se tiendra dans le courant de la première semaine de ce mois de juillet. Ce programme se focalisera d’abord sur la filière litchi.Financé par la commission européenne, le programme EDES contribue à l’amélioration des aliments et à la réduction de la pauvreté dans les pays de l’Afrique-Caraïbes et Pacifique. Ce programme vise essentiellement à renforcer leur politique nationale ou régionale de sécurité sanitaire des aliments basés sur l’analyse du système existant.  Car  EDES a pour objectif de contribuer à la pérennisation des flux de produits alimentaires d'origine animale et végétale vers l'UE ou au niveau régional notamment par le biais de l'intégration accrue des petits producteurs dans la chaîne d'approvisionnement. L’appui aux organisations de petits producteurs actives au sein des filières d’exportation, contribue à lutter contre leur marginalisation et à renforcer leur capacité à produire des aliments sûrs destinés aux consommateurs locaux, régionaux et internationaux.Du 14 au 20 janvier dernier, la commission de l’union européenne a effectué une mission à Madagascar afin d’évaluer les potentialités de la Grande île, en réponse à la requête du gouvernement qui a été introduite en 2012. Cette mission a constaté la volonté de l’Etat d’entreprendre des actions pour l’amélioration de la qualité des produits agro-alimentaires et également d’accroître leur compétitivité dans le but d’envahir le marché international de nos produits locaux. Dans ce dessein, EDES œuvrera pour apporter un appui à la mise en œuvre de ces actions. Ainsi, trois points seront à souligner dans cette contribution à savoir la conduite de l’analyse du système de sécurité sanitaire des aliments (SSSA), la redynamisation de la plateforme de concentration publique privée pour la filière litchi et la rédaction d’un guide de contrôle et organisation des contrôles officiels de cette filière. Par ailleurs, EDES s’engage notamment à fournir  l’assistance technique nécessaire en matière de méthodologie, de formation et de mise à disposition de personnes ressources externes appropriées.Faut-il noter que dans les pays ACP, EDES contribue non seulement à la promotion de la production du litchi, comme le cas de Madagascar mais aussi à la production de cacao, à l’amélioration des denrées alimentaires végétales et animales et bien d’autres encore… Recueillis par Faly R.",
                            Sous_categorie="economie",
                            Autheur="Rajaonarison Faly"},
                        new ActualitesItem { Id = Guid.NewGuid().ToString(),
                            Images ="hajo_1_0.jpg",
                            Titre = "Décision prise pour les étudiants en Chine",
                            Description ="Paiement des arriérés de frais de scolarité, retrait du statut de boursier, rapatriement d’étudiants et examen au cas par cas, telles ont été les décisions prises par le conseil du gouvernement hier, (02/07/2013), pour les étudiants boursiers en Chine.",
                             Contenue ="Le conseil du gouvernement d’hier (02/07/2013) a approuvé la communication relative d’un protocole d’entente entre la République malgache et le programme EDES-COLEAP concernant l’amélioration du système de sécurité sanitaire des aliments à Madagascar. Le lancement officiel de ce programme se tiendra dans le courant de la première semaine de ce mois de juillet. Ce programme se focalisera d’abord sur la filière litchi.Financé par la commission européenne, le programme EDES contribue à l’amélioration des aliments et à la réduction de la pauvreté dans les pays de l’Afrique-Caraïbes et Pacifique. Ce programme vise essentiellement à renforcer leur politique nationale ou régionale de sécurité sanitaire des aliments basés sur l’analyse du système existant.  Car  EDES a pour objectif de contribuer à la pérennisation des flux de produits alimentaires d'origine animale et végétale vers l'UE ou au niveau régional notamment par le biais de l'intégration accrue des petits producteurs dans la chaîne d'approvisionnement. L’appui aux organisations de petits producteurs actives au sein des filières d’exportation, contribue à lutter contre leur marginalisation et à renforcer leur capacité à produire des aliments sûrs destinés aux consommateurs locaux, régionaux et internationaux.Du 14 au 20 janvier dernier, la commission de l’union européenne a effectué une mission à Madagascar afin d’évaluer les potentialités de la Grande île, en réponse à la requête du gouvernement qui a été introduite en 2012. Cette mission a constaté la volonté de l’Etat d’entreprendre des actions pour l’amélioration de la qualité des produits agro-alimentaires et également d’accroître leur compétitivité dans le but d’envahir le marché international de nos produits locaux. Dans ce dessein, EDES œuvrera pour apporter un appui à la mise en œuvre de ces actions. Ainsi, trois points seront à souligner dans cette contribution à savoir la conduite de l’analyse du système de sécurité sanitaire des aliments (SSSA), la redynamisation de la plateforme de concentration publique privée pour la filière litchi et la rédaction d’un guide de contrôle et organisation des contrôles officiels de cette filière. Par ailleurs, EDES s’engage notamment à fournir  l’assistance technique nécessaire en matière de méthodologie, de formation et de mise à disposition de personnes ressources externes appropriées.Faut-il noter que dans les pays ACP, EDES contribue non seulement à la promotion de la production du litchi, comme le cas de Madagascar mais aussi à la production de cacao, à l’amélioration des denrées alimentaires végétales et animales et bien d’autres encore… Recueillis par Faly R.",
                            Sous_categorie="societe",
                            Autheur="Rajaonarison Faly"},
                        new ActualitesItem { Id = Guid.NewGuid().ToString(),
                            Images ="pauvrety.jpg",
                            Titre = "9 Malgaches sur 10 sont pauvres",
                            Description ="La banque mondiale a sorti officiellement hier une nouvelle publication sur les enjeux du développement à Madagascar. Ainsi, selon ce nouveau rapport de la Banque mondiale, Madagascar est actuellement quatre fois plus pauvre qu’en 1960 malgré le fait qu’il a des ressources naturelles qui pourraient être exploitées pour soutenir l’économie. Ainsi, d’après la responsable de la Banque mondiale, Haleh Bridi, neuf Malgaches sur dix vivent aujourd'hui avec moins de 2 dollars par jour.",
                            Contenue ="Le conseil du gouvernement d’hier (02/07/2013) a approuvé la communication relative d’un protocole d’entente entre la République malgache et le programme EDES-COLEAP concernant l’amélioration du système de sécurité sanitaire des aliments à Madagascar. Le lancement officiel de ce programme se tiendra dans le courant de la première semaine de ce mois de juillet. Ce programme se focalisera d’abord sur la filière litchi.Financé par la commission européenne, le programme EDES contribue à l’amélioration des aliments et à la réduction de la pauvreté dans les pays de l’Afrique-Caraïbes et Pacifique. Ce programme vise essentiellement à renforcer leur politique nationale ou régionale de sécurité sanitaire des aliments basés sur l’analyse du système existant.  Car  EDES a pour objectif de contribuer à la pérennisation des flux de produits alimentaires d'origine animale et végétale vers l'UE ou au niveau régional notamment par le biais de l'intégration accrue des petits producteurs dans la chaîne d'approvisionnement. L’appui aux organisations de petits producteurs actives au sein des filières d’exportation, contribue à lutter contre leur marginalisation et à renforcer leur capacité à produire des aliments sûrs destinés aux consommateurs locaux, régionaux et internationaux.Du 14 au 20 janvier dernier, la commission de l’union européenne a effectué une mission à Madagascar afin d’évaluer les potentialités de la Grande île, en réponse à la requête du gouvernement qui a été introduite en 2012. Cette mission a constaté la volonté de l’Etat d’entreprendre des actions pour l’amélioration de la qualité des produits agro-alimentaires et également d’accroître leur compétitivité dans le but d’envahir le marché international de nos produits locaux. Dans ce dessein, EDES œuvrera pour apporter un appui à la mise en œuvre de ces actions. Ainsi, trois points seront à souligner dans cette contribution à savoir la conduite de l’analyse du système de sécurité sanitaire des aliments (SSSA), la redynamisation de la plateforme de concentration publique privée pour la filière litchi et la rédaction d’un guide de contrôle et organisation des contrôles officiels de cette filière. Par ailleurs, EDES s’engage notamment à fournir  l’assistance technique nécessaire en matière de méthodologie, de formation et de mise à disposition de personnes ressources externes appropriées.Faut-il noter que dans les pays ACP, EDES contribue non seulement à la promotion de la production du litchi, comme le cas de Madagascar mais aussi à la production de cacao, à l’amélioration des denrées alimentaires végétales et animales et bien d’autres encore… Recueillis par Faly R.",
                            Sous_categorie="economie",
                            Autheur="Rajaonarison Faly"},
                        new ActualitesItem { Id = Guid.NewGuid().ToString(),
                            Images ="Beriziky.jpg",
                            Titre = "Beriziky à Antsohihy",
                            Description ="Le Premier Ministre, Jean Omer Beriziky a été convié le 05 juillet 2013 à l’inauguration de deux bâtiments administratifs régionaux dans la ville d’Antsohihy, région Sofia, à savoir le bureau régional de la décentralisation et la maison des élus.",
                            Contenue ="Le conseil du gouvernement d’hier (02/07/2013) a approuvé la communication relative d’un protocole d’entente entre la République malgache et le programme EDES-COLEAP concernant l’amélioration du système de sécurité sanitaire des aliments à Madagascar. Le lancement officiel de ce programme se tiendra dans le courant de la première semaine de ce mois de juillet. Ce programme se focalisera d’abord sur la filière litchi.Financé par la commission européenne, le programme EDES contribue à l’amélioration des aliments et à la réduction de la pauvreté dans les pays de l’Afrique-Caraïbes et Pacifique. Ce programme vise essentiellement à renforcer leur politique nationale ou régionale de sécurité sanitaire des aliments basés sur l’analyse du système existant.  Car  EDES a pour objectif de contribuer à la pérennisation des flux de produits alimentaires d'origine animale et végétale vers l'UE ou au niveau régional notamment par le biais de l'intégration accrue des petits producteurs dans la chaîne d'approvisionnement. L’appui aux organisations de petits producteurs actives au sein des filières d’exportation, contribue à lutter contre leur marginalisation et à renforcer leur capacité à produire des aliments sûrs destinés aux consommateurs locaux, régionaux et internationaux.Du 14 au 20 janvier dernier, la commission de l’union européenne a effectué une mission à Madagascar afin d’évaluer les potentialités de la Grande île, en réponse à la requête du gouvernement qui a été introduite en 2012. Cette mission a constaté la volonté de l’Etat d’entreprendre des actions pour l’amélioration de la qualité des produits agro-alimentaires et également d’accroître leur compétitivité dans le but d’envahir le marché international de nos produits locaux. Dans ce dessein, EDES œuvrera pour apporter un appui à la mise en œuvre de ces actions. Ainsi, trois points seront à souligner dans cette contribution à savoir la conduite de l’analyse du système de sécurité sanitaire des aliments (SSSA), la redynamisation de la plateforme de concentration publique privée pour la filière litchi et la rédaction d’un guide de contrôle et organisation des contrôles officiels de cette filière. Par ailleurs, EDES s’engage notamment à fournir  l’assistance technique nécessaire en matière de méthodologie, de formation et de mise à disposition de personnes ressources externes appropriées.Faut-il noter que dans les pays ACP, EDES contribue non seulement à la promotion de la production du litchi, comme le cas de Madagascar mais aussi à la production de cacao, à l’amélioration des denrées alimentaires végétales et animales et bien d’autres encore… Recueillis par Faly R.",
                            Sous_categorie="economie",
                            Autheur="Rajaonarison Faly"},
                        };

                foreach (ActualitesItem item in _items)
                {
                    items.Add(item);
                }
                //---------------------------------------------------------------------------------------------------------------------------
                // FIN STATIC
                //---------------------------------------------------------------------------------------------------------------------------

                */
            //-----------------------------------------------------------------------------------------------------------------------------
            //FIN ACTUALITES
            //-----------------------------------------------------------------------------------------------------------------------------
            isInitialized = true;
        }
    }
}
