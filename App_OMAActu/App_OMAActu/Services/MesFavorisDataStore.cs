﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App_OMAActu.Models;
using System.Net.Http.Headers;
using System.Net.Http;
using Xamarin.Forms;
using System.Web;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Net;
using WebApiContrib.Formatting;
using System.Diagnostics;

[assembly: Dependency(typeof(App_OMAActu.Services.MesFavorisDataStore))]
namespace App_OMAActu.Services
{
    public class MesFavorisDataStore : InterfaceMesFavoris<MesFavorisItem>
    {
        bool isInitialized;
        List<MesFavorisItem> items;

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> AddItemAsync(MesFavorisItem item)
        {
            await InitializeAsync();

            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> UpdateItemAsync(MesFavorisItem item)
        {
            await InitializeAsync();

            var _item = items.Where((MesFavorisItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);
            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> DeleteItemAsync(MesFavorisItem item)
        {
            await InitializeAsync();

            var _item = items.Where((MesFavorisItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);

            return await Task.FromResult(true);
        }

        public async Task<MesFavorisItem> GetItemAsync(string id)
        {
            await InitializeAsync();

            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }
        //--------------------------------------------------------------------------------------------------------------------------------
        public async Task<IEnumerable<MesFavorisItem>> GetItemsAsync(bool forceRefresh = false)
        {
            await InitializeAsync();

            return await Task.FromResult(items);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> PullLatestAsync()
        {
            return Task.FromResult(true);
        }
        //-------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> SyncAsync()
        {
            return Task.FromResult(true);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task InitializeAsync()
        {
            if (isInitialized)
                return;

            items = new List<MesFavorisItem>();
           

            //---------------------------------------------------------------------------------------------------------------------------
            // STATIC
            //---------------------------------------------------------------------------------------------------------------------------
            var _items = new List<MesFavorisItem>();

            foreach (MesFavorisItem item in _items)
            {
                items.Add(item);
            }
            //---------------------------------------------------------------------------------------------------------------------------
            // FIN STATIC
            //---------------------------------------------------------------------------------------------------------------------------


            //-----------------------------------------------------------------------------------------------------------------------------
            //FIN ACTUALITES
            //-----------------------------------------------------------------------------------------------------------------------------
            isInitialized = true;
        }
    }
}
