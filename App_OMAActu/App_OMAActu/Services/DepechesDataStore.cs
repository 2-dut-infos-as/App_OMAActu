﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App_OMAActu.Models;
using System.Net.Http.Headers;
using System.Net.Http;
using Xamarin.Forms;
using System.Web;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Net;
using WebApiContrib.Formatting;
using System.Diagnostics;
using Java.IO;
using static Android.Resource;

[assembly: Dependency(typeof(App_OMAActu.Services.DepechesDataStore))]
namespace App_OMAActu.Services
{
    public class DepechesDataStore : InterfaceDepeches<DepechesItem>
    {
        bool isInitialized;
        List<DepechesItem> items;
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> AddItemAsync(DepechesItem item)
        {
            await InitializeAsync();

            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> UpdateItemAsync(DepechesItem item)
        {
            await InitializeAsync();

            var _item = items.Where((DepechesItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);
            items.Add(item);

            return await Task.FromResult(true);
        }
        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task<bool> DeleteItemAsync(DepechesItem item)
        {
            await InitializeAsync();

            var _item = items.Where((DepechesItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);

            return await Task.FromResult(true);
        }

        public async Task<DepechesItem> GetItemAsync(string id)
        {
            await InitializeAsync();

            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }
        //--------------------------------------------------------------------------------------------------------------------------------
        public async Task<IEnumerable<DepechesItem>> GetItemsAsync(bool forceRefresh = false)
        {
            await InitializeAsync();

            return await Task.FromResult(items);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> PullLatestAsync()
        {
            return Task.FromResult(true);
        }
        //-------------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------------
        public Task<bool> SyncAsync()
        {
            return Task.FromResult(true);
        }
        //--------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------------
        public async Task InitializeAsync()
        {
            if (isInitialized)
                return;

            items = new List<DepechesItem>();
            //---------------------------------------------------------------------------------------------------------------------------
            // DYNAMIQUE
            //---------------------------------------------------------------------------------------------------------------------------

            HttpClient client = new HttpClient();

            // Download string.
            string value = await client.GetStringAsync("https://api.myjson.com/bins/daxmn");


            var _items = JsonConvert.DeserializeObject<List<DepechesItem>>(value);


            foreach (var obj in _items)
            {
                DepechesItem actualites = obj;

                actualites.Id = Guid.NewGuid().ToString();

            }


            foreach (DepechesItem item in _items)
            {
                items.Add(item);
            }
            //---------------------------------------------------------------------------------------------------------------------------
            // FIN DYNAMIQUE
            //---------------------------------------------------------------------------------------------------------------------------

            isInitialized = true;
        }
    }
}
