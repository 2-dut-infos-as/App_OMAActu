﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;

using App_OMAActu.Helpers;
using App_OMAActu.Models;
using App_OMAActu.Views;
using Xamarin.Forms;

namespace App_OMAActu.ViewModels
{
    public class VideosViewModel : BaseViewModel
    {
        public ObservableRangeCollection<VideosItem> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public VideosViewModel()
        {
            Title = "Les Vidéos";
            Items = new ObservableRangeCollection<VideosItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, VideosItem>(this, "AddItem", async (obj, item) =>
            {
                var _item = item as VideosItem;
                Items.Add(_item);
                await DataStoreVideos.AddItemAsync(_item);
            });
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStoreVideos.GetItemsAsync(true);
                Items.ReplaceRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}