﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

using App_OMAActu.Helpers;
using App_OMAActu.Models;
using App_OMAActu.Views;

using Xamarin.Forms;


namespace App_OMAActu.ViewModels
{
    public class DepechesViewModel : BaseViewModel
    {
        public ObservableRangeCollection<DepechesItem> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public DepechesViewModel()
        {
            Title = "Information en continue";
            Items = new ObservableRangeCollection<DepechesItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, DepechesItem>(this, "AddItem", async (obj, item) =>
            {
                var _item = item as DepechesItem;
                Items.Add(_item);
                await DataStoreDepeches.AddItemAsync(_item);
            });
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStoreDepeches.GetItemsAsync(true);
                Items.ReplaceRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}