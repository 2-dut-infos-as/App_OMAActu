﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

using App_OMAActu.Helpers;
using App_OMAActu.Models;
using App_OMAActu.Views;

using Xamarin.Forms;

namespace App_OMAActu.ViewModels
{
    public class ActualitesViewModel : BaseViewModel
    {
        public ObservableRangeCollection<ActualitesItem> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ActualitesViewModel()
        {
            Title = "Les Actualités";
            //Logo = "orange_logo.png";
            Items = new ObservableRangeCollection<ActualitesItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, ActualitesItem>(this, "AddItem", async (obj, item) =>
            {
                var _item = item as ActualitesItem;
                Items.Add(_item);
                await DataStoreActualites.AddItemAsync(_item);
            });

 
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStoreActualites.GetItemsAsync(true);
                Items.ReplaceRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}