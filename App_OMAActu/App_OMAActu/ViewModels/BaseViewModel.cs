﻿using App_OMAActu.Helpers;
using App_OMAActu.Models;
using App_OMAActu.Services;

using Xamarin.Forms;

namespace App_OMAActu.ViewModels
{
    public class BaseViewModel : ObservableObject
    {
        /// <summary>
        /// Get the azure service instance
        /// </summary>
        public InterfaceActualites<ActualitesItem> DataStoreActualites => DependencyService.Get<InterfaceActualites<ActualitesItem>>();
        public InterfaceRegions<RegionsItem> DataStoreRegions => DependencyService.Get<InterfaceRegions<RegionsItem>>();
        public InterfaceVideos<VideosItem> DataStoreVideos => DependencyService.Get<InterfaceVideos<VideosItem>>();
        public InterfaceDepeches<DepechesItem> DataStoreDepeches => DependencyService.Get<InterfaceDepeches<DepechesItem>>();
        public InterfaceMesFavoris<MesFavorisItem> DataStoreMesFavoris => DependencyService.Get<InterfaceMesFavoris<MesFavorisItem>>();

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }
        /// <summary>
        /// Private backing field to hold the title
        /// </summary>
        string title = string.Empty;
        string logo = string.Empty;
        /// <summary>
        /// Public property to set and get the title of the item
        /// </summary>
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }
        public string Logo
        {
            get { return logo; }
            set { SetProperty(ref logo, value); }
        }
    }
}

