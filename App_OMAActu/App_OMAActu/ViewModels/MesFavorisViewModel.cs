﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;
using App_OMAActu.Helpers;
using App_OMAActu.Models;
using App_OMAActu.Views;



namespace App_OMAActu.ViewModels
{
    class MesFavorisViewModel : BaseViewModel
    {
        public ObservableRangeCollection<MesFavorisItem> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public MesFavorisViewModel()
        {
            Title = "Mes Favoris";
            //Logo = "orange_logo.png";
            Items = new ObservableRangeCollection<MesFavorisItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, MesFavorisItem>(this, "AddItem", async (obj, item) =>
            {
                var _item = item as MesFavorisItem;
                Items.Add(_item);
                await DataStoreMesFavoris.AddItemAsync(_item);
            });
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStoreMesFavoris.GetItemsAsync(true);
                Items.ReplaceRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}