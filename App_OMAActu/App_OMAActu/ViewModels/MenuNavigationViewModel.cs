﻿
using System.Collections.Generic;

using Xamarin.Forms;
using App_OMAActu.Views;

namespace App_OMAActu.ViewModels
{
    public class MenuNavigationViewModel : ContentPage
    {
        public ListView ListView { get { return listView; } }

        ListView listView;
        public MenuNavigationViewModel()
        {

            var menuNavigationPageItems = new List<MenuNavigationItem>();
            menuNavigationPageItems.Add(new MenuNavigationItem
            {
                Title = "Actualités",
                IconSource = "actualites.png",
                TargetType = typeof(ActualitesViewModel)
            });

            menuNavigationPageItems.Add(new MenuNavigationItem
            {
                Title = "Info en continue",
                IconSource = "Information.png",
                TargetType = typeof(DepechesViewModel)
            });


            listView = new ListView
            {
                ItemsSource = menuNavigationPageItems,
                ItemTemplate = new DataTemplate(() => {
                    var imageCell = new ImageCell();
                    imageCell.SetBinding(TextCell.TextProperty, "Title");
                    imageCell.SetBinding(ImageCell.ImageSourceProperty, "IconSource");
                    return imageCell;
                }),
                VerticalOptions = LayoutOptions.FillAndExpand,
                SeparatorVisibility = SeparatorVisibility.None
            };

            Padding = new Thickness(0, 40, 0, 0);
            Icon = "hamburger.png";
            Title = "Personal Organiser";
            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children = {
                    listView
                }
            };
        }
    }
}
