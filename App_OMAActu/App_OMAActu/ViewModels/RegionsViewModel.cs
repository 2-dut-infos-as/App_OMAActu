﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

using App_OMAActu.Helpers;
using App_OMAActu.Models;
using App_OMAActu.Views;

using Xamarin.Forms;
namespace App_OMAActu.ViewModels
{
    public class RegionsViewModel : BaseViewModel
    {
        public ObservableRangeCollection<RegionsItem> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public RegionsViewModel()
        {
            Title = "Les Actu par Regions";
            Items = new ObservableRangeCollection<RegionsItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, RegionsItem>(this, "AddItem", async (obj, item) =>
            {
                var _item = item as RegionsItem;
                Items.Add(_item);
                await DataStoreRegions.AddItemAsync(_item);
            });
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStoreRegions.GetItemsAsync(true);
                Items.ReplaceRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}