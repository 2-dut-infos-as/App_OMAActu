﻿using App_OMAActu.Models;

namespace App_OMAActu.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public ActualitesItem ItemActualites { get; set; }
        public RegionsItem ItemRegions { get; set; }
        public VideosItem ItemVideos { get; set; }
        public MesFavorisItem ItemMesFavoris { get; set; }
        public DepechesItem ItemDepeches { get; set; }
        public ItemDetailViewModel(ActualitesItem item = null)
        {
            Title = item.titre;
            ItemActualites = item;
        }
        public ItemDetailViewModel(RegionsItem item = null)
        {
            Title = item.titre;
            ItemRegions = item;
        }
        public ItemDetailViewModel(VideosItem item = null)
        {
            Title = item.titre;
            ItemVideos = item;
        }
        public ItemDetailViewModel(MesFavorisItem item = null)
        {
            Title = item.Titre;
            ItemMesFavoris = item;
        }
        public ItemDetailViewModel(DepechesItem item = null)
        {
            Title = item.titre;
            ItemDepeches = item;
        }

        int quantity = 1;
        public int Quantity
        {
            get { return quantity; }
            set { SetProperty(ref quantity, value); }
        }
    }
}