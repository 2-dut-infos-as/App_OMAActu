﻿using App_OMAActu.Views;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace App_OMAActu
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            BottomBarPage bottomBarPage = new BottomBarPage();

            SetMainPage();

            //----------------------------------------------------------------------------------------------------------

            /*
            string[] tabTitles = { "Favorites", "Friends", "Nearby", "Recents", "Restaurants" };
            string[] tabColors = { null, "#5D4037", "#7B1FA2", "#FF5252", "#FF9800" };
            int[] tabBadgeCounts = { 0, 1, 5, 3, 4 };
            string[] tabBadgeColors = { "#000000", "#FF0000", "#000000", "#000000", "#000000" };

            for (int i = 0; i < tabTitles.Length; ++i)
            {
                string title = tabTitles[i];
                string tabColor = tabColors[i];
                int tabBadgeCount = tabBadgeCounts[i];
                string tabBadgeColor = tabBadgeColors[i];

                FileImageSource icon = (FileImageSource)FileImageSource.FromFile(string.Format("ic_{0}.png", title.ToLowerInvariant()));

                // create tab page
                var tabPage = new ActualitesPage()
                {
                    Title = title,
                    Icon = icon
                };

                // set tab color
                if (tabColor != null)
                {
                    BottomBarPageExtensions.SetTabColor(tabPage, Color.FromHex(tabColor));
                }

                // Set badges
                BottomBarPageExtensions.SetBadgeCount(tabPage, tabBadgeCount);
                BottomBarPageExtensions.SetBadgeColor(tabPage, Color.FromHex(tabBadgeColor));

                // set label based on title
                //tabPage.UpdateLabel();

                // add tab pag to tab control
                bottomBarPage.Children.Add(tabPage);
            }
            //------------------------------------------------------------------------------------------------------------
            MainPage = bottomBarPage;
            */
            //MainPage = new BarPage();

        }

        public static void SetMainPage()
        {   
            Current.MainPage = new TabbedPage
            {
                Children =
                {
                    new NavigationPage(new ActualitesPage())
                    {
                        Title = "Actu",
                        Icon = Device.OnPlatform("tab_feed.png",null,null)
                    },
                    new NavigationPage(new RegionsPage())
                    {
                        Title = "Régions",
                        Icon = Device.OnPlatform("tab_about.png",null,null)
                    },
                    new NavigationPage(new VideosPage())
                    {
                        Title = "Vidéos",
                        Icon = Device.OnPlatform("tab_about.png",null,null)
                    },
                     new NavigationPage(new DepechesPage())
                    {
                        Title = "Fil info",
                        Icon = Device.OnPlatform("tab_about.png",null,null)
                    },
                     new NavigationPage(new MesFavorisPage())
                    {
                        Title = "Favoris",
                        Icon = Device.OnPlatform("tab_about.png",null,null)
                    },
                    
                }
            };

        }
    }
}
