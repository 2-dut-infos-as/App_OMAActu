﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace App_OMAActu.Models
{
    public class ActualitesItem : BaseDataObject
    {
        public object date { get; set; }
        public string formated_date { get; set; }
        public string url { get; set; }
        public string type { get; set; }
        public int cid { get; set; }
        public string uiUrl { get; set; }
        public object pratiques { get; set; }
        public object sousRubriqueResponses { get; set; }
        public string titre { get; set; }
        public string description { get; set; }
        public string contenue { get; set; }
        public string autheur { get; set; }
        public string categorie { get; set; }
        public string sous_categorie { get; set; }
        public List<string> images { get; set; }
        public static HtmlWebViewSource WebViewSource(string data)
        {
           
         return new HtmlWebViewSource { Html = data};
            
        }

        public HtmlWebViewSource WebSource
        {
            get;set;

        }
    }
}
    

