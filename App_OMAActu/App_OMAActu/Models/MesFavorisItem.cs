﻿using System.Collections.Generic;

namespace App_OMAActu.Models
{
    public class MesFavorisItem : BaseDataObject
    {
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        long date;
        public long Date
        {
            get { return date; }
            set { SetProperty(ref date, value); }
        }
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        string formated_date = string.Empty;
        public string Formated_date
        {
            get { return formated_date; }
            set { SetProperty(ref formated_date, value); }
        }
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        string url = string.Empty;
        public string Url
        {
            get { return url; }
            set { SetProperty(ref url, value); }
        }
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        string type = string.Empty;
        public string Type
        {
            get { return type; }
            set { SetProperty(ref type, value); }
        }
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        int cid;
        public int Cid
        {
            get { return cid; }
            set { SetProperty(ref cid, value); }
        }
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        string uiUrl = string.Empty;
        public string UiUrl
        {
            get { return uiUrl; }
            set { SetProperty(ref uiUrl, value); }
        }
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        object pratiques;
        public object Pratiques
        {
            get { return pratiques; }
            set { SetProperty(ref pratiques, value); }
        }
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        object sousRubriqueResponses;
        public object SousRubriqueResponses
        {
            get { return sousRubriqueResponses; }
            set { SetProperty(ref sousRubriqueResponses, value); }
        }
        //--------------------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------------------
        string titre = string.Empty;
        public string Titre
        {
            get { return titre; }
            set { SetProperty(ref titre, value); }
        }
        //---------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------------------
        string description = string.Empty;
        public string Description
        {
            get { return description; }
            set { SetProperty(ref description, value); }
        }
        //---------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        string contenue = string.Empty;
        public string Contenue
        {
            get { return contenue; }
            set { SetProperty(ref contenue, value); }
        }
        //---------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        string autheur = string.Empty;
        public string Autheur
        {
            get { return autheur; }
            set { SetProperty(ref autheur, value); }
        }
        //---------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        string categorie = string.Empty;
        public string Categorie
        {
            get { return categorie; }
            set { SetProperty(ref categorie, value); }
        }
        //---------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        string sous_categorie = string.Empty;
        public string Sous_categorie
        {
            get { return sous_categorie; }
            set { SetProperty(ref sous_categorie, value); }
        }
        //---------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
        string images = string.Empty;
        public string Images
        {
            get { return images; }
            set { SetProperty(ref images, value); }
        }
        //---------------------------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------------------------
    }
}
